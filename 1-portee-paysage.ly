\version "2.24.2"

#(set-global-staff-size 24)

\header {
  tagline = ##f
}

\score {
  {
    \repeat unfold 10 { s1 \break }
  }
  \layout {
    indent = 0\in
    \context {
      \Staff
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
  }
}


\paper {
  #(set-paper-size "a4landscape")
  ragged-last-bottom = ##f
  line-width = 267
  left-margin = 15
  bottom-margin = 15
  top-margin = 15
}